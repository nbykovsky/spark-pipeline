import json
import sys
from zipfile import ZipFile
import os
import shutil
import argparse
from paramiko import SSHClient
from scp import SCPClient
import yaml


def load_config(file_name):
    with open(file_name) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    return config


def get_all_file_paths(directory: str):
    file_paths = []

    for root, directories, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)

    return file_paths


def create_bundle(args):
    jobs_paths = get_all_file_paths(args.src_jobs_dir)
    utils_paths = get_all_file_paths(args.src_utils_dir)
    lib_paths = get_all_file_paths(args.trg_tmp_lib_dir)

    with ZipFile(args.trg_zip, 'w') as z:

        for file in utils_paths:
            z.write(file, arcname=os.path.relpath(file, args.src_main_dir))

        for file in jobs_paths:
            # stripping "project" folder from archive
            # ex: src/main/jobs/project/... -->> jobs/...
            path_to_job_files = os.path.join(args.src_main_dir, 'jobs', args.project)
            path_inside_jobs_project_dir = os.path.relpath(file, path_to_job_files)
            path_in_archive = os.path.join('jobs', path_inside_jobs_project_dir)
            z.write(file, arcname=path_in_archive)

        for file in lib_paths:
            z.write(file, arcname=os.path.relpath(file, args.trg_tmp_dir))

        z.write(args.trg_tmp_conf, arcname=args.config_name)
        z.writestr('__init__.py', " ")


def copy_files(args):
    shutil.copyfile(args.src_dag, args.trg_dag)
    shutil.copyfile(args.src_run_job, args.trg_run_job)


def build(args):

    # clean and create structure
    try:
        shutil.rmtree(args.trg_dir)
    except FileNotFoundError:
        pass

    os.makedirs(args.trg_dir)
    os.makedirs(args.trg_bin_dir)
    os.makedirs(args.trg_dags_dir)
    os.makedirs(args.trg_tmp_lib_dir)

    # generate artifacts
    install_dependencies(args.packages)
    generate_config(args)
    create_bundle(args)
    copy_files(args)

    # delete tmp files
    shutil.rmtree(args.trg_tmp_dir)


def install_dependencies(packages):
    cmd = 'pip install {} -t {}'.format(" ".join(packages), args.trg_tmp_lib_dir)
    os.system(cmd)


def generate_config(args):
    with open(args.trg_tmp_conf, 'w') as f:
        json.dump(args.conf, f, indent=2)


def test(args):
    import unittest
    sys.path.append(args.src_main_dir)
    loader = unittest.TestLoader()
    suite = loader.discover(args.tests_dir)

    runner = unittest.TextTestRunner()
    runner.run(suite)


def deploy(args):
    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.connect(args.host, username=args.user, look_for_keys=False)
    with SCPClient(ssh.get_transport()) as scp:
        scp.put(args.trg_dag, args.deploy_dags)
        scp.put(args.trg_run_job, args.deploy_bin)
        scp.put(args.trg_zip, args.deploy_bin)


def run(args):
    if args.command == 'build':
        test(args)
        build(args)
    elif args.command == 'test':
        test(args)
    elif args.command == 'deploy':
        deploy(args)
    elif args.command == 'all':
        test(args)
        build(args)
        deploy(args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('command', choices=['build', 'test', 'deploy', 'all'])
    parser.add_argument('--config', '-c', default='config.yaml')
    parser.add_argument('--env', '-e', choices=['dev', 'test', 'prod'], required=True)
    parser.add_argument('--project', '--prj', '-p', required=True)
    args = parser.parse_args()

    config = load_config(args.config)

    args.project_dir = config["project-dir"]
    args.conf = config['env'][args.project][args.env]
    args.packages = config['packages']
    args.host = config['env'][args.project][args.env]['host']
    args.user = config['env'][args.project][args.env]['user']

    args.dag_name = '{}.py'.format(args.project)
    args.run_job_name = 'run_job.py'
    args.config_name = "config.json"
    args.zip_name = '{}.zip'.format(args.project)

    args.src_dag = os.path.join(args.project_dir, 'src/main/dags', args.dag_name)
    args.src_main_dir = os.path.join(args.project_dir, 'src/main')
    args.src_jobs_dir = os.path.join(args.src_main_dir, 'jobs/', args.project)
    args.src_utils_dir = os.path.join(args.src_main_dir, 'utils/')
    args.src_run_job = os.path.join(args.src_main_dir, args.run_job_name)

    args.trg_dir = os.path.join(args.project_dir, 'target')
    args.trg_bin_dir = os.path.join(args.trg_dir, 'bin')
    args.trg_dags_dir = os.path.join(args.trg_dir, 'dags')
    args.trg_tmp_dir = os.path.join(args.trg_dir, 'tmp')
    args.trg_tmp_lib_dir = os.path.join(args.trg_tmp_dir, 'lib')
    args.trg_tmp_conf = os.path.join(args.trg_tmp_dir,  args.config_name)
    args.trg_zip = os.path.join(args.trg_bin_dir, args.zip_name)
    args.trg_run_job = os.path.join(args.trg_bin_dir, args.run_job_name)
    args.trg_dag = os.path.join(args.trg_dags_dir, args.dag_name)

    args.deploy_dir = config['env'][args.project][args.env]['deploy-dir']
    args.deploy_dags = os.path.join(args.deploy_dir, 'dags/')
    args.deploy_bin = os.path.join(args.deploy_dir, 'bin/')

    args.tests_dir = 'src/test'

    run(args)