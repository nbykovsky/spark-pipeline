from datetime import date
from typing import Dict, Callable, Any

from utils.config_parser import ConfigParser
from utils.file_manager import FileManager
from utils.job import Job


class Init(Job):

    def __init__(self, file_manager: FileManager, run_date: date):
        self._file_manager = file_manager
        self._run_date = run_date

    def run(self):
        folder_name = self._run_date.strftime("%Y-%m-%d")
        self._file_manager.mkdir(folder_name)

    @staticmethod
    def constructor(*args, config: Dict[str, str] = {}):
        vrs = ConfigParser(config)\
            .with_date('--start')\
            .parse(*args)

        file_manager = FileManager.hdfs(Init.ss, vrs.hdfs_url, vrs.base_path)
        return Init(file_manager, vrs.start)

    @staticmethod
    def process(*args, config: Dict[str, str] = {}):
        Init.constructor(*args, config=config).run()

