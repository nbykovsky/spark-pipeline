import os
from datetime import date
from typing import Dict

from utils.config_parser import ConfigParser
from utils.job import Job
from utils.partition_manager import PartitionManager
from utils.reader import Reader
from utils.writer import Writer
from pyspark.sql.window import Window
import pyspark.sql.functions as F


class Returns(Job):

    def __init__(self,
                 reader: Reader,
                 writer: Writer,
                 symbol: str,
                 prt_mth: date):
        self._reader = reader
        self._writer = writer
        self._symbol = symbol
        self._prt_mnt = prt_mth

    def run(self):
        pm = PartitionManager(self._symbol, self._prt_mnt)
        df_current = self._reader.read(pm.current)
        df_prev = self._reader.read(pm.prev)

        df = df_current.union(df_prev)
        w = Window.orderBy(df.DT.asc())
        df = df.withColumn('OPEN_PREV', F.lag('OPEN').over(w))
        df = df.withColumn('RETURN', (df.OPEN - df.OPEN_PREV) / df.OPEN_PREV)\
            .filter(F.month(df.DT) == self._prt_mnt.month)\
            .select("DT", "RETURN")

        name = PartitionManager(self._symbol + "_returns", self._prt_mnt).current
        self._writer.write(df, name)

    @staticmethod
    def constructor(*args, config: Dict[str, str] = {}) -> 'Returns':
        vrs = ConfigParser(config)\
            .with_date('--start')\
            .with_string('--symbol')\
            .parse(*args)
        reader = Reader.parquet(Returns.ss, vrs.base_path)
        writer = Writer.hdfs(vrs.base_path, fmt='parquet', mode='overwrite')
        return Returns(reader, writer, vrs.symbol, vrs.start)

    @staticmethod
    def process(*args, config: Dict[str, str] = {}) -> None:
        Returns.constructor(*args, config=config).run()