import os
from argparse import Namespace
from datetime import date
from typing import Dict, Any, Callable

from utils.config_parser import ConfigParser
from utils.job import Job
from utils.partition_manager import PartitionManager
from utils.reader import Reader
from utils.writer import Writer
from dateutil import relativedelta


class Downloader(Job):

    def __init__(self,
                 reader: Reader,
                 writer: Writer,
                 base_path: str,
                 symbol: str,
                 start_date: date):
        self._reader = reader
        self._writer = writer
        self._base_path = base_path
        self._symbol = symbol
        self._start_date = start_date

    def run(self):
        df = self._reader.read(self._symbol)
        file_path = PartitionManager(self._symbol, self._start_date).current
        self._writer.write(df, file_path)

    @staticmethod
    def constructor(*args, config: Dict[str, str] = {}):
        vrs = ConfigParser(config)\
            .with_date('--start')\
            .with_string('--symbol')\
            .parse(*args)

        end = vrs.start + relativedelta.relativedelta(months=1)
        reader = Reader.finam(Downloader.ss, vrs.start, end)
        writer = Writer.hdfs(vrs.base_path)

        return Downloader(reader, writer, vrs.base_path, vrs.symbol, vrs.start)

    @staticmethod
    def process(*args, config: Dict[str, str] = {}):
        Downloader.constructor(*args, config=config).run()

