from datetime import date
from typing import Dict, List

from dateutil.relativedelta import relativedelta
from pyspark.sql.types import StructType, StructField, DateType, DoubleType

from utils.config_parser import ConfigParser
from utils.job import Job
from utils.partition_manager import PartitionManager
from utils.reader import Reader
from utils.writer import Writer


class Datamart(Job):

    def __init__(self,
                 reader: Reader,
                 writer: Writer,
                 init_dt: date,
                 dt: date,
                 symbols: List[str]):
        self._reader = reader
        self._writer = writer
        self._init_dt = init_dt
        self._dt = dt
        self._symbols = symbols

    def run(self):

        schema = StructType([
            StructField('DT', DateType()),
            StructField('RETURN', DoubleType())
        ])

        datamart = None

        for symbol in self._symbols:
            acc = self.ss.createDataFrame([], schema=schema)
            for part_path in PartitionManager('{}_returns'.format(symbol), self._dt, init_dt=self._init_dt):
                acc = acc.union(self._reader.read(part_path))
            acc = acc.withColumnRenamed('RETURN', symbol)
            if datamart:
                datamart = datamart.join(acc, "DT")
            else:
                datamart = acc

        self._writer.write(datamart, 'datamart.parquet')

    @staticmethod
    def constructor(*args, config: Dict[str, str] = {}) -> 'Datamart':
        vrs = ConfigParser(config)\
            .with_date('--dt')\
            .with_date('--initdt')\
            .with_strings('--symbols')\
            .parse(*args)

        reader = Reader.parquet(Datamart.ss, vrs.base_path)
        writer = Writer.hdfs(vrs.base_path, fmt='parquet', mode='overwrite')
        return Datamart(reader, writer, vrs.initdt + relativedelta(months=1), vrs.dt, vrs.symbols)

    @staticmethod
    def process(*args, config: Dict[str, str] = {}) -> None:
        Datamart.constructor(*args, config=config).run()