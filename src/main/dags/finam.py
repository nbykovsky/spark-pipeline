from datetime import datetime

from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.operators.python_operator import ShortCircuitOperator
from airflow.utils.dates import days_ago
from dateutil.parser import parse

PROJECT_NAME = 'finam'

py_file = '/opt/airflow/bin/{}.zip'.format(PROJECT_NAME)
app_file = '/opt/airflow/bin/run_job.py'

symbols = [
    'USDRUB_TOD',
    'SBRF-6.20',
    'BR-10.20'
]

init_date = datetime(2019, 9, 1)

project_args = ['--project', PROJECT_NAME]


args = {
    'owner': 'Airflow',
    # 'start_date':  datetime(2019, 1, 1) #,
    # 'schedule_interval': '@monthly',
}

dag = DAG(
    dag_id='finam_dag',
    default_args=args,
    tags=['nb'],
    catchup=True,
    start_date=init_date,
    schedule_interval='@monthly'
)

init = SparkSubmitOperator(
    task_id='init',
    application=app_file,
    py_files=py_file,
    deploy_mode='cluster',
    application_args=project_args + ['--module', 'init', '--start', '{{ ds }}'],
    dag=dag
)

smbls = []
for s in symbols:
    smbls.append('--symbols')
    smbls.append(s)

datamart = SparkSubmitOperator(
    task_id='datamart',
    application=app_file,
    py_files=py_file,
    deploy_mode='cluster',
    application_args=project_args + ['--module', 'datamart',
                                     '--dt', '{{ ds }}',
                                     '--initdt', init_date.strftime('%Y-%m-%d'),
                                     ] + smbls,
    dag=dag
)

for symbol in symbols:

    download_quotes = SparkSubmitOperator(
        task_id='download_quotes_%s' % symbol,
        application=app_file,
        py_files=py_file,
        deploy_mode='cluster',
        application_args=project_args + ['--module', 'downloader', '--start', '{{ ds }}', '--symbol', symbol],
        dag=dag
    )

    def is_first_run(run_date):
        return not parse(run_date).date() == init_date.date()


    condition = ShortCircuitOperator(
        task_id='skip_first_execution_%s' % symbol,
        dag=dag,
        python_callable=is_first_run,
        op_args=['{{ ds }}']
    )

    calculate_returns = SparkSubmitOperator(
        task_id='calculate_returns_%s' % symbol,
        application=app_file,
        py_files=py_file,
        deploy_mode='cluster',
        application_args=project_args + ['--module', 'returns', '--start', '{{ ds }}', '--symbol', symbol],
        depends_on_past=True,
        dag=dag
    )

    init >> download_quotes >> condition >> calculate_returns >> datamart
