from pyspark.sql import SparkSession


def get_spark(app_name: str) -> SparkSession:
    return SparkSession.builder.appName(app_name).getOrCreate()


class SparkContextMixIn:

    ss = get_spark(__name__)

    def __del__(self):
        self.ss.stop()
