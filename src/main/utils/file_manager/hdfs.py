import os

from pyspark.sql import SparkSession

from utils.file_manager import FileManager


class FileManagerHDFS(FileManager):

    def __init__(self, ss: SparkSession, hdfs_url: str, base_path: str):
        self._hdfs_url = hdfs_url
        self._base_path = base_path
        self._ss = ss

    def mkdir(self, name: str) -> None:
        sc = self._ss.sparkContext
        URI = sc._gateway.jvm.java.net.URI
        Path = sc._gateway.jvm.org.apache.hadoop.fs.Path
        FileSystem = sc._gateway.jvm.org.apache.hadoop.fs.FileSystem
        fs = FileSystem.get(URI(self._hdfs_url), sc._jsc.hadoopConfiguration())
        fs.mkdirs(Path(os.path.join(self._base_path, name)))