from abc import ABC, abstractmethod

from pyspark.sql import SparkSession


class FileManager(ABC):

    @abstractmethod
    def mkdir(self, name: str) -> None: ...

    @staticmethod
    def hdfs(ss: SparkSession, hdfs_url: str, base_path: str) -> 'FileManager':
        from utils.file_manager.hdfs import FileManagerHDFS
        return FileManagerHDFS(ss, hdfs_url, base_path)

    @staticmethod
    def test(mkdir_callback=None) -> 'FileManager':
        from utils.file_manager.test import FileManagerTest
        return FileManagerTest(mkdir_callback=mkdir_callback)