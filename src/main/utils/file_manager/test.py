
from utils.file_manager import FileManager


class FileManagerTest(FileManager):

    def __init__(self, mkdir_callback=None):
        self._mkdir = mkdir_callback

    def mkdir(self, name: str) -> None:
        if self._mkdir:
            self._mkdir(name)