import argparse
import json
from datetime import datetime
from typing import Dict, Any


class ConfigParser:

    def __init__(self, config: Dict[str, str]):
        self._parser = argparse.ArgumentParser()
        self._config = config

    def with_date(self, name):
        self._parser.add_argument(name, type=lambda x: datetime.strptime(x, "%Y-%m-%d").date(),
                                  metavar="YYYY-MM-DD", help="%s date" % name)
        return self

    def with_string(self, name):
        self._parser.add_argument(name, type=str)
        return self

    def with_strings(self, name):
        self._parser.add_argument(name, type=str, action='append')
        return self

    def parse(self, *args) -> argparse.Namespace:
        ns = self._parser.parse_args(args) if args else self._parser.parse_args()
        for k, v in self._config.items():
            setattr(ns, k.replace('-', '_'), v)
        return ns
