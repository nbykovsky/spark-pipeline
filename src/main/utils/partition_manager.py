import os
from datetime import date, datetime
from dateutil.relativedelta import relativedelta


class PartitionManager:

    ext = {
        'prq': 'parquet'
    }

    def __init__(self, name: str, dt: date, fmt: str = 'prq', init_dt: date = datetime(1900, 1, 1).date()):
        self._name = name
        self._first_day_of_month = dt.replace(day=1)
        self._ext = self.ext[fmt]
        self._file_name = "{}.{}".format(self._name, self._ext)
        self._init_dt = init_dt

    @property
    def current(self) -> str:
        return os.path.join(self._first_day_of_month.strftime("%Y-%m-%d"), self._file_name)

    @property
    def prev(self) -> str:
        prev_month = self._first_day_of_month - relativedelta(months=1)
        return os.path.join(prev_month.strftime("%Y-%m-%d"), self._file_name)

    @property
    def next(self) -> str:
        next_month = self._first_day_of_month + relativedelta(months=1)
        return os.path.join(next_month.strftime("%Y-%m-%d"), self._file_name)

    def __iter__(self):
        return self

    def __next__(self):
        if self._first_day_of_month < self._init_dt:
            raise StopIteration
        next_path = self.current
        self._first_day_of_month -= relativedelta(months=1)
        return next_path
