import os

from pyspark.sql import DataFrame

from utils.writer import Writer


class WriterHDFS(Writer):

    fmts = ('parquet', )
    modes = ('overwrite',)

    def __init__(self,
                 base_path: str,
                 fmt: str,
                 mode: str):
        assert fmt in self.fmts
        assert mode in self.modes
        self._fmt = fmt
        self._mode = mode
        self._base_path = base_path

    def write(self, df: DataFrame, path: str) -> None:
        absolute_path = os.path.join(self._base_path, path)
        df.write.format(self._fmt).mode(self._mode).save(absolute_path)
