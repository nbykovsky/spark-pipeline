from abc import ABC, abstractmethod
from typing import Dict

from pyspark.sql import DataFrame


class Writer(ABC):

    @abstractmethod
    def write(self, df: DataFrame, path: str) -> None: ...

    @staticmethod
    def hdfs(base_path: str, fmt: str = 'parquet', mode: str = 'overwrite') -> 'Writer':
        from utils.writer.hdfs import WriterHDFS
        return WriterHDFS(base_path, fmt, mode)

    @staticmethod
    def test(dfs: Dict[str, DataFrame]) -> 'Writer':
        from utils.writer.test import WriterTest
        return WriterTest(dfs)