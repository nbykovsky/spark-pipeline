from typing import Dict

from pyspark.sql import DataFrame

from utils.writer import Writer


class WriterTest(Writer):

    def __init__(self, dfs: Dict[str, DataFrame]):
        self._dfs = dfs

    def write(self, df: DataFrame, path: str) -> None:
        try:
            assert df.schema == self._dfs[path].schema
        except AssertionError as e:
            print('Actual schema: ', df.schema)
            print('Expected schema: ', self._dfs[path].schema)
            raise e

        return None
