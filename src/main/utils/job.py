from abc import ABC, abstractmethod

from utils.spark_context import get_spark


class Job(ABC):

    ss = get_spark(__name__)

    @staticmethod
    @abstractmethod
    def process(*args, config=None) -> None: ...

    def __del__(self):
        self.ss.stop()

