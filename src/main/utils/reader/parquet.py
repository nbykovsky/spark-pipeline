import os
from pyspark.sql import SparkSession, DataFrame
from utils.reader import Reader


class ReaderParquet(Reader):

    def __init__(self, ss: SparkSession, base_path: str):
        self._ss = ss
        self._base_path = base_path

    def read(self, path: str) -> DataFrame:
        absolute_path = os.path.join(self._base_path, path)
        return self._ss.read.format('parquet').load(absolute_path)