from datetime import date

from dateutil.parser import parse
from finam.export import Market, Exporter
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.types import StructType, StructField, DoubleType, IntegerType, DateType
from utils.reader import Reader


class ReaderFinam(Reader):

    markets = {
        'USDRUB_TOD': Market.CURRENCIES,
        'SBRF-6.20': Market.FUTURES,
        'BR-10.20': Market.FUTURES
    }

    schema = StructType([
        StructField('DT', DateType()),
        StructField('OPEN', DoubleType()),
        StructField('HIGH', DoubleType()),
        StructField('LOW', DoubleType()),
        StructField('CLOSE', DoubleType()),
        StructField('VOL', IntegerType())
    ])

    def __init__(self, ss: SparkSession, start_date: date, end_date: date) -> None:
        self._ss = ss
        self._start_date = start_date
        self._end_date = end_date

    def read(self, path: str) -> DataFrame:
        exporter = Exporter()
        market = self.markets[path]
        rub = exporter.lookup(name=path, market=market)

        if not len(rub):
            raise ValueError('Symbol {} not found'.format(path))
        elif len(rub) > 1:
            raise ValueError('There are {} symbols {}'.format(len(rub), path))

        pd_df = exporter.download(rub.index[0], market=market, start_date=self._start_date,
                                  end_date=self._end_date)
        pd_df.reset_index(level=0, inplace=True)
        df = self._ss.createDataFrame(pd_df, schema=ReaderFinam.schema)
        return df

