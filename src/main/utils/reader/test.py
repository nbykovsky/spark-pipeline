from typing import List, Dict

from pyspark.sql import DataFrame

from utils.reader import Reader


class ReaderTest(Reader):

    def __init__(self, dfs: Dict[str, DataFrame]):
        self._dfs = dfs

    def read(self, path: str) -> DataFrame:
        return self._dfs[path]