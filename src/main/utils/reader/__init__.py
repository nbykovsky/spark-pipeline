
from abc import ABC, abstractmethod
from datetime import date
from typing import Dict

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql.types import StructType


class Reader(ABC):

    @abstractmethod
    def read(self, path: str) -> DataFrame: ...

    @staticmethod
    def parquet(ss: SparkSession, base_path: str) -> 'Reader':
        from utils.reader.parquet import ReaderParquet
        return ReaderParquet(ss, base_path)

    @staticmethod
    def finam(ss: SparkSession, start_date: date, end_date: date) -> 'Reader':
        from utils.reader.finam import ReaderFinam
        return ReaderFinam(ss, start_date, end_date)

    @staticmethod
    def test(dfs: Dict[str, DataFrame]) -> 'Reader':
        from utils.reader.test import ReaderTest
        return ReaderTest(dfs)
