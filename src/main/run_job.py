import argparse
import sys
import zipfile
import json
from typing import Any, Dict
from zipimport import zipimporter

CONFIG_NAME = 'config.json'


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--project', type=str)
    parser.add_argument('--module', type=str)
    # parser.add_argument('rest', nargs=argparse.ZERO_OR_MORE)
    return parser


def load_config(zip_archive: str) -> Dict[str, str]:
    with zipfile.ZipFile(zip_archive, 'r') as f:
        config = f.open(CONFIG_NAME)
    return json.load(config)


if __name__ == '__main__':
    parser = create_parser()
    args, rest = parser.parse_known_args()

    zip_archive = '{}.zip'.format(args.project)
    module_path = 'jobs/{}'.format(args.module)

    config = load_config(zip_archive)

    # appending path to access zip internals form job body
    sys.path.append(zip_archive)

    # accessing job
    zip_importer = zipimporter(zip_archive)
    job_module = zip_importer.load_module(module_path)
    job_class = getattr(job_module, args.module.capitalize())
    job_class.process(*rest, config=config)