import unittest

from dateutil.parser import parse
from pyspark.sql.types import StructType, StructField, DateType, DoubleType

from jobs.finam.datamart import Datamart
from utils.reader import Reader
from utils.reader.parquet import ReaderParquet
from utils.spark_context import get_spark
from utils.writer import Writer
from utils.writer.hdfs import WriterHDFS


class TestDatamart(unittest.TestCase):

    def setUp(self) -> None:
        self.ss = get_spark("test")

    def tearDown(self) -> None:
        self.ss.stop()

    def test_constructor(self):
        dm = Datamart.constructor(*
                                  ['--symbols', 'A',
                                   '--symbols', 'B',
                                   '--dt',      '2020-03-01',
                                   '--initdt',  '2020-01-01'],
                                  config={
                                      'base-path': 'path/'
                                  })
        dm.ss = self.ss
        self.assertTrue(isinstance(dm._reader, ReaderParquet))
        self.assertTrue(isinstance(dm._writer, WriterHDFS))
        self.assertEqual(dm._reader._base_path, 'path/')
        self.assertEqual(dm._writer._base_path, 'path/')
        self.assertEqual(dm._init_dt, parse('2020-02-01').date())
        self.assertEqual(dm._dt, parse('2020-03-01').date())
        self.assertEqual(dm._symbols, ['A', 'B'])

    def test_run(self):

        def get_df(s, x):
            return self.ss.createDataFrame([
                (parse(s).date(), x),
            ], schema=StructType([
                        StructField('DT', DateType()),
                        StructField('RETURN', DoubleType())
                    ]))

        data_in = {
            '2020-03-01/A_returns.parquet': get_df('2020-03-14', 1.0),
            '2020-03-01/B_returns.parquet': get_df('2020-03-14', 2.0),
            '2020-02-01/A_returns.parquet': get_df('2020-02-14', 3.0),
            '2020-02-01/B_returns.parquet': get_df('2020-02-14', 4.0),
            '2020-01-01/A_returns.parquet': get_df('2020-01-14', 5.0),
            '2020-01-01/B_returns.parquet': get_df('2020-01-14', 6.0),
        }
        reader = Reader.test(data_in)
        data_out = {
            'datamart.parquet': self.ss.createDataFrame(
                [
                    (parse('2020-03-14').date(), 1.0, 2.0),
                    (parse('2020-02-14').date(), 3.0, 4.0),
                    (parse('2020-01-14').date(), 5.0, 6.0),
                ],
                schema=StructType([
                    StructField('DT', DateType()),
                    StructField('A', DoubleType()),
                    StructField('B', DoubleType())
                ]))
        }
        writer = Writer.test(data_out)
        dm = Datamart(reader, writer, parse('2020-01-01'), parse('2020-03-01'), ['A', 'B'])
        dm.ss = self.ss
        dm.run()