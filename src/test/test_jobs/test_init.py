import unittest

from jobs.finam.init import Init
from utils.file_manager import FileManager
from utils.file_manager.hdfs import FileManagerHDFS
from dateutil.parser import parse


class TestInit(unittest.TestCase):

    def test_constructor(self):
        init = Init.constructor(*["--start", "2010-10-11"],
                                config={
                                    "hdfs-url": "hdfs",
                                    "base-path": "path"
                                })
        self.assertTrue(isinstance(init._file_manager, FileManagerHDFS))
        self.assertEqual(init._file_manager._hdfs_url, "hdfs")
        self.assertEqual(init._file_manager._base_path, "path")

    def test_run(self):

        called = False

        def mkdir_cb(name):
            nonlocal called
            self.assertEqual(name, "2010-10-11")
            called = True

        file_manager = FileManager.test(mkdir_cb)
        init = Init(file_manager, parse('2010-10-11').date())
        init.run()
        self.assertTrue(called)