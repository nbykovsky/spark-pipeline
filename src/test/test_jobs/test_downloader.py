import unittest

from dateutil.parser import parse
from pyspark.sql.types import IntegerType

from jobs.finam.downloader import Downloader
from utils.reader import Reader
from utils.reader.finam import ReaderFinam
from utils.spark_context import get_spark
from utils.writer import Writer
from utils.writer.hdfs import WriterHDFS


class TestDownloader(unittest.TestCase):

    def setUp(self) -> None:
        self.ss = get_spark("test")

    def tearDown(self) -> None:
        self.ss.stop()

    def test_constructor(self):
        downloader = Downloader.constructor(*['--start', '2010-01-01', '--symbol', 'USDRUB'],
                                            config={"base_path": "abc"})
        self.assertTrue(isinstance(downloader._reader, ReaderFinam))
        self.assertTrue(isinstance(downloader._writer, WriterHDFS))
        self.assertEqual(downloader._reader._start_date, parse('2010-01-01').date())
        self.assertEqual(downloader._reader._end_date, parse('2010-02-01').date())
        self.assertEqual(downloader._symbol, 'USDRUB')
        self.assertEqual(downloader._base_path, 'abc')

    def test_constructor_date(self):
        downloader = Downloader.constructor(*['--start', '2020-03-18', '--symbol', 'USDRUB'],
                                            config={"base_path": "abc"})
        self.assertEqual(downloader._reader._end_date, parse('2020-04-18').date())

    def test_run(self):
        df = self.ss.createDataFrame([1, 2, 3], schema=IntegerType())
        reader = Reader.test({"USDRUB": df})
        writer = Writer.test({"2020-03-01/USDRUB.parquet": df})
        downloader = Downloader(reader, writer, "x", "USDRUB", parse('2020-03-01'))
        downloader.run()

