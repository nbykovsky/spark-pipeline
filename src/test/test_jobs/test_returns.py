import unittest

from dateutil.parser import parse
from pyspark.sql.types import StructType, DateType, StructField, DoubleType

from jobs.finam.returns import Returns
from utils.reader import Reader
from utils.reader.finam import ReaderFinam
from utils.reader.parquet import ReaderParquet
from utils.spark_context import get_spark
from utils.writer import Writer
from utils.writer.hdfs import WriterHDFS


class TestReturns(unittest.TestCase):

    def setUp(self) -> None:
        self.ss = get_spark("test")

    def tearDown(self) -> None:
        self.ss.stop()

    def test_constructor(self):
        rts = Returns.constructor(*['--start', '2020-02-01', '--symbol', 'USDRUB'],
                                  config={"base_path": "/path"})
        self.assertTrue(isinstance(rts._reader, ReaderParquet))
        self.assertTrue(isinstance(rts._writer, WriterHDFS))
        self.assertEqual(rts._reader._base_path, "/path")
        self.assertEqual(rts._symbol, "USDRUB")
        self.assertEqual(rts._prt_mnt, parse('2020-02-01').date())

    def test_run(self):
        data_prev = [
            (parse('2020-02-14'), 1.0, 63.61, 63.36, 63.4425, 562858000),
            (parse('2020-02-15'), 2.0, 63.61, 63.36, 63.4425, 562858000),
            (parse('2020-02-16'), 4.0, 63.61, 63.36, 63.4425, 562858000),
            (parse('2020-02-17'), 8.0, 63.61, 63.36, 63.4425, 562858000),
        ]
        data_curr = [
            (parse('2020-03-14'), 16.0, 63.61, 63.36, 63.4425, 562858000),
            (parse('2020-03-15'), 32.0, 63.61, 63.36, 63.4425, 562858000),
            (parse('2020-03-16'), 64.0, 63.61, 63.36, 63.4425, 562858000),
            (parse('2020-03-17'), 128.0, 63.61, 63.36, 63.4425, 562858000),
        ]
        reader = Reader.test({
            "2020-02-01/USDRUB.parquet": self.ss.createDataFrame(data_prev, schema=ReaderFinam.schema),
            "2020-03-01/USDRUB.parquet": self.ss.createDataFrame(data_curr, schema=ReaderFinam.schema),
        })
        ret = [
            (parse('2020-03-14'), 1.0),
            (parse('2020-03-15'), 1.0),
            (parse('2020-03-16'), 1.0),
            (parse('2020-03-17'), 1.0)
        ]
        schema = StructType([
            StructField('DT', DateType()),
            StructField('RETURN', DoubleType())
        ])
        writer = Writer.test({
            "2020-03-01/USDRUB_returns.parquet": self.ss.createDataFrame(ret, schema=schema)
        })

        rts = Returns(reader, writer, 'USDRUB', parse('2020-03-01'))
        rts.run()

