import unittest

from dateutil.parser import parse

from utils.partition_manager import PartitionManager


class TestPartitionManager(unittest.TestCase):

    def test_partition_manager(self):
        pm = PartitionManager("X", parse('2010-04-14'), 'prq')
        self.assertEqual(pm.current, "2010-04-01/X.parquet")
        self.assertEqual(pm.prev, "2010-03-01/X.parquet")
        self.assertEqual(pm.next, "2010-05-01/X.parquet")

    def test_iter(self):
        pm = PartitionManager("X", parse('2010-04-14'), 'prq', parse('2010-01-14'))
        self.assertEqual(list(pm), ["2010-04-01/X.parquet", "2010-03-01/X.parquet",
                                    "2010-02-01/X.parquet"])

