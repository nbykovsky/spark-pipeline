import unittest

from utils.config_parser import ConfigParser


class TestConfigParser(unittest.TestCase):

    def test_with_strings(self):
        parser = ConfigParser({}).with_strings('--test')
        self.assertEqual(parser.parse(*['--test', 'abc', '--test', 'bcd']).test, ['abc', 'bcd'])