from dateutil.parser import parse

from utils.reader import Reader
from utils.spark_context import get_spark

if __name__ == '__main__':
    ss = get_spark('test')
    finam_reader = Reader.finam(ss,
                                parse("2020-03-01"),
                                parse("2020-04-01"))
    finam_reader.read('BR-10.20')
    ss.stop()
